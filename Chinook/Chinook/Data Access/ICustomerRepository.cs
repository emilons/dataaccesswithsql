﻿using System;
using System.Collections.Generic;
using Chinook.Models;

namespace Chinook.Data_Access
{
    /// <summary>
    /// Interface handling data retrieval and manipulation.
    /// NOTE: Method specific comments are provided on each method in CustomerRepository.cs
    /// </summary>
    public interface ICustomerRepository
    {
        public List<Customer> GetAllCustomers();
        public Customer GetCustomerById(string id);
        public Customer GetCustomerByName(string firstName, string lastName);
        public List<Customer> GetRangeOfCustomers(string from, string to);
        public bool AddNewCustomer(Customer customer);
        public bool UpdateCustomerById(Customer customer, int id);
        public List<CustomerCountry> GetCustomerCountryRelationDescending();
        public List<CustomerSpender> GetCustomerInvoiceRelationDescending();
        public CustomerGenre GetCustomerGenreRelation(string id);
    }
}

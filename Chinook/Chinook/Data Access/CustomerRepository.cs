﻿using Chinook.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook.Data_Access
{
    /// <summary>
    /// Handles the implementation of sql queries to the database.
    /// </summary>
    public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// Adds a new customer to the database.
        /// </summary>
        /// <param name="customer">The Customer object provided by the model to be added in the database.</param>
        /// <returns>True if customer was added successfully.</returns>
        public bool AddNewCustomer(Customer customer)
        {
            bool success = false;
            string query = "INSERT INTO CUSTOMER(FirstName, LastName, Country, PostalCode, Phone, Email)"
                + " VALUES(@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(query, connection))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery()> 0 ? true : false;
                    }
                }
            }
            catch (SqlException) { }
            return success;
        }

        /// <summary>
        /// Select all customers in the Customer table.
        /// </summary>
        /// <returns>List of Customer objects.</returns>
        public List<Customer> GetAllCustomers()
        {
            List<Customer> customers = new List<Customer>();
            string query = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email"
                + " FROM Customer";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(query, connection))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new Customer();
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                customer.Phone = reader.IsDBNull(5) ? null : reader.GetString(5);
                                customer.Email = reader.IsDBNull(6) ? null : reader.GetString(6);
                                customers.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (SqlException) { }
            return customers;
        }

        /// <summary>
        /// Select customer by id.
        /// </summary>
        /// <param name="id">CustomerId of the customer.</param>
        /// <returns>Customer object of the selected customer.</returns>
        public Customer GetCustomerById(string id)
        {
            Customer customer = new Customer();
            string query = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email"
                + " FROM Customer"
                + " WHERE CustomerId = @CustomerId";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(query, connection))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                customer.Phone = reader.IsDBNull(5) ? null : reader.GetString(5);
                                customer.Email = reader.IsDBNull(6) ? null : reader.GetString(6);
                            }
                        }
                    }
                }
            }
            catch (SqlException) {}
            return customer;
        }

        /// <summary>
        /// Select customer by full name.
        /// </summary>
        /// <param name="firstName">First name of the customer.</param>
        /// <param name="lastName">Last name of the customer</param>
        /// <returns>Customer object of the selected customer.</returns>
        public Customer GetCustomerByName(string firstName, string lastName)
        {
            Customer customer = new Customer();
            string query = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email"
                + " FROM Customer"
                + " WHERE FirstName = @FirstName AND LastName = @LastName";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(query, connection))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", firstName);
                        cmd.Parameters.AddWithValue("@LastName", lastName);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                customer.Phone = reader.IsDBNull(5) ? null : reader.GetString(5);
                                customer.Email = reader.IsDBNull(6) ? null : reader.GetString(6);
                            }
                        }
                    }
                }
            }
            catch (SqlException) { }
            return customer;
        }

        /// <summary>
        /// Select a list of customers from the Customer table.
        /// </summary>
        /// <param name="from">Id of the selection offset.</param>
        /// <param name="to">Id of the selection limit.</param>
        /// <returns>List of Customer objects in the given range.</returns>
        public List<Customer> GetRangeOfCustomers(string from, string to)
        {
            List<Customer> customers = new List<Customer>();
            string query = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email"
                + " FROM Customer"
                + " WHERE CustomerId BETWEEN @FromValue AND @ToValue";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(query, connection))
                    {
                        cmd.Parameters.AddWithValue("@FromValue", from);
                        cmd.Parameters.AddWithValue("@ToValue", to);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new Customer();
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                customer.Phone = reader.IsDBNull(5) ? null : reader.GetString(5);
                                customer.Email = reader.IsDBNull(6) ? null : reader.GetString(6);
                                customers.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (SqlException) { }
            return customers;
        }

        public bool UpdateCustomerById(Customer customer, int id)
        {
            bool success = false;
            string query = "UPDATE Customer SET " +
                "FirstName = @FirstName, LastName = @LastName, Country = @Country, " +
                "PostalCode = @PostalCode, Phone = @Phone, Email = @Email " +
                "WHERE CustomerId = @CustomerId";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(query, connection))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException) { }
            return success;
        }

        /// <summary>
        /// Select number of customers in each country.
        /// </summary>
        /// <returns>List of CustomerCountry objects, with countries and their number of subscribers.</returns>
        public List<CustomerCountry> GetCustomerCountryRelationDescending()
        {
            List<CustomerCountry> customersInCountry = new List<CustomerCountry>();
            string query = "SELECT Country, COUNT(CustomerId) AS CustomerCount FROM Customer GROUP BY Country ORDER BY CustomerCount DESC";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(query, connection))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerCountry relation = new CustomerCountry();
                                relation.Country = reader.GetString(0);
                                relation.CustomerCount = reader.GetInt32(1);
                                customersInCountry.Add(relation);
                            }
                        }
                    }
                }
            }
            catch (SqlException) { }
            return customersInCountry;
        }

        /// <summary>
        /// Select all customers ordered by the highest spenders.
        /// </summary>
        /// <returns>List of CustomerSpender objects, with each customers invoice total value.</returns>
        public List<CustomerSpender> GetCustomerInvoiceRelationDescending()
        {
            List<CustomerSpender> highestSpenders = new List<CustomerSpender>();
            string query = "SELECT Customer.CustomerId, Customer.FirstName, Customer.LastName, Invoice.Total " +
                "FROM Customer INNER JOIN Invoice " +
                "ON Customer.CustomerId = Invoice.CustomerId " +
                "ORDER BY Invoice.Total DESC";
            try 
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(query, connection))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerSpender relation = new CustomerSpender();
                                relation.CustomerId = reader.GetInt32(0);
                                relation.FirstName = reader.GetString(1);
                                relation.LastName = reader.GetString(2);
                                relation.InvoiceTotal = reader.GetDecimal(3);
                                highestSpenders.Add(relation);
                            }
                        }
                    }
                }
            }
            catch (SqlException) { }
            return highestSpenders;
        }

        /// <summary>
        /// Selects a customer and see their most played genre of music.
        /// </summary>
        /// <param name="id">CustomerId of the customer.</param>
        /// <returns>CustomerGenre object, containing customer id, name, most popular genre and said genre amount.</returns>
        public CustomerGenre GetCustomerGenreRelation(string id)
        {
            CustomerGenre customer = new CustomerGenre();
            customer.GenreName = new List<string>();
            string query = "SELECT TOP 1 WITH TIES Genre.Name, " +
                "COUNT(Genre.Name) AS GenreCount, Customer.CustomerId, " +
                "Customer.FirstName, Customer.LastName FROM Customer " +
                "INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId " +
                "INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId " +
                "INNER JOIN Track ON InvoiceLine.TrackId = Track.TrackId " +
                "INNER JOIN Genre ON Track.GenreId = Genre.GenreId " +
                "WHERE Customer.CustomerId = @CustomerId " +
                "GROUP BY Genre.Name, Customer.CustomerId, Customer.FirstName, Customer.LastName " +
                "ORDER BY GenreCount DESC";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(query, connection))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            int count = 0;
                            while (reader.Read())
                            {
                                if (count == 0)
                                {
                                    customer.GenreCount = reader.GetInt32(1);
                                    customer.CustomerId = reader.GetInt32(2);
                                    customer.FirstName = reader.GetString(3);
                                    customer.LastName = reader.GetString(4);
                                }
                                customer.GenreName.Add(reader.GetString(0));
                                count++;
                            }
                        }
                    }
                }
            }
            catch (SqlException) { }
            return customer;
        }
    }
}

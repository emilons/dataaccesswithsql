﻿using Microsoft.Data.SqlClient;
using System;

namespace Chinook
{
    /// <summary>
    /// Handles retrieval of the correct database connection information.
    /// </summary>
    public class ConnectionHelper
    {
        /// <summary>
        /// Sets environment variables and builds a string of connection information.
        /// </summary>
        /// <returns>Returns the string of connection information.</returns>
        public static string GetConnectionString()
        {
            DotEnv.SetEnv();  // Sets the environment variables
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = Environment.GetEnvironmentVariable("SqlServerAddress"); // Address is found in the .env file
            builder.InitialCatalog = "Chinook";
            builder.IntegratedSecurity = true;
            return builder.ConnectionString;
        }
    }
}

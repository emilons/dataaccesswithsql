﻿using System;
using System.IO;

namespace Chinook
{
    /// <summary>
    /// Handles reading of environment variables from .env file.
    /// </summary>
    public static class DotEnv
    {
        /// <summary>
        /// Loads the .env file and sets all values as EnvironmentVariables.
        /// </summary>
        /// <param name="filePath">Path to folder where the .env file is located</param>
        public static void Load(string filePath)
        {
            if (!File.Exists(filePath))
                return;

            foreach (var line in File.ReadAllLines(filePath))
            {
                var parts = line.Split(
                    '=',
                    StringSplitOptions.RemoveEmptyEntries);

                if (parts.Length != 2)
                    continue;

                Environment.SetEnvironmentVariable(parts[0], parts[1]);
            }
        }
        /// <summary>
        /// Set environment variables
        /// </summary>
        public static void SetEnv()
        {
            var root = Directory.GetCurrentDirectory();
            var dotenv = Path.Combine(root, ".env"); // .env is located in the working directory
            DotEnv.Load(dotenv);
        }

    }
}

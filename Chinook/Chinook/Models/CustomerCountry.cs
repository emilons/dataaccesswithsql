﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook.Models
{
    /// <summary>
    /// Model of the Customer/Country relation.
    /// </summary>
    public class CustomerCountry : Customer
    {
        public int CustomerCount { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook.Models
{
    /// <summary>
    /// Model of the Customer/Invoice relation.
    /// </summary>
    public class CustomerSpender : Customer
    {
        public decimal InvoiceTotal { get; set; }
    }
}

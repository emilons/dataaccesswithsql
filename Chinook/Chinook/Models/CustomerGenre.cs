﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook.Models
{
    /// <summary>
    /// Model of the Customer/Genre relation.
    /// </summary>
    public class CustomerGenre : Customer
    {
        public List<string> GenreName { get; set; }
        public int GenreCount { get; set; }
    }
}

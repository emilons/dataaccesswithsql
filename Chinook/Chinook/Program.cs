﻿using System;
using System.IO;
using Microsoft.Data.SqlClient;
using Chinook.Data_Access;
using Chinook.Models;
using System.Text;
using System.Collections.Generic;

namespace Chinook
{

    class Program
    {
        
        public static void Main(string[] args)
        {
            CustomerRepository rep = new();
            
            // Test cases

            //TestSelectAllCustomers(rep);
            //TestSelectCustomer(rep);
            //TestSelectRangeOfCustomers(rep);
            //TestInsert(rep);
            //TestUpdate(rep);
            //TestCustomerCountryRelation(rep);
            //TestCustomerSpenderRelation(rep);
            //TestCustomerGenreRelation(rep);
        }

        /// <summary>
        /// Helper function to print a list of customers to console.
        /// </summary>
        /// <param name="customers"></param>
        private static void PrintCustomers(IEnumerable<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }

        /// <summary>
        /// Helper function to print a single customer to console.
        /// </summary>
        /// <param name="customer"></param>
        private static void PrintCustomer(Customer customer)
        {
            StringBuilder s = new StringBuilder();
            s.Append(
                $"ID: {customer.CustomerId}, " +
                $"{customer.FirstName} {customer.LastName}, " +
                $"{customer.Country}, " +
                $"PostalCode: {customer.PostalCode}, " +
                $"Phone: {customer.Phone}, " +
                $"Email: {customer.Email}");
            Console.WriteLine(s);
        }

        // As the code handles and interacts with an external database, all methods are tested manually,
        // either by checking the console for correct output or by checking the database itself for changes
        // in MS SQL Server Management Studio.

        // Test methods
        static void TestSelectAllCustomers(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetAllCustomers());
        }

        static void TestSelectCustomer(ICustomerRepository repository)
        {
            PrintCustomer(repository.GetCustomerById("10"));
            PrintCustomer(repository.GetCustomerByName("Eduardo", "Martins"));
        }

        static void TestSelectRangeOfCustomers(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetRangeOfCustomers("1", "10"));
        }

        static void TestInsert(ICustomerRepository repository)
        {
            Customer customer = new Customer()
            {
                FirstName = "Arnt",
                LastName = "Berger",
                Country = "Norway",
                PostalCode = "0123",
                Phone = "99995555",
                Email = "arntmann@abc.no"
            };
            Console.WriteLine(repository.AddNewCustomer(customer));
        }

        static void TestUpdate(ICustomerRepository repository)
        {
            int id = 60;
            Customer customer = new Customer()
            {
                FirstName = "Gnarnt",
                LastName = "Gnerger",
                Country = "Norway",
                PostalCode = "3210",
                Phone = "55559999",
                Email = "Gnarntmann@abc.no"
            };
            Console.WriteLine(repository.UpdateCustomerById(customer, id));
        }

        static void TestCustomerCountryRelation(ICustomerRepository repository)
        {
            Console.WriteLine("Country: CustomerCount");
            foreach (CustomerCountry relation in repository.GetCustomerCountryRelationDescending())
            {
                Console.WriteLine($"{relation.Country}: {relation.CustomerCount}");
            }
        }

        static void TestCustomerSpenderRelation(ICustomerRepository repository)
        {
            Console.WriteLine("CustomerId, FirstName, LastName, InvoiceTotal");
            foreach (CustomerSpender relation in repository.GetCustomerInvoiceRelationDescending())
            {
                Console.WriteLine($"{relation.CustomerId}," +
                    $" {relation.FirstName}," +
                    $" {relation.LastName}," +
                    $" {relation.InvoiceTotal}");
            }
        }

        static void TestCustomerGenreRelation(ICustomerRepository repository)
        {
            CustomerGenre relation = repository.GetCustomerGenreRelation("12");
            StringBuilder s = new StringBuilder();
            s.Append($"Genre: ");
            foreach (string genreName in relation.GenreName)
            {
                s.Append(genreName + ", ");
            }
            s.Append($"GenreCount: {relation.GenreCount}, " +
                $"CustomerId: {relation.CustomerId}, " +
                $"{relation.FirstName} {relation.LastName}");
            Console.WriteLine(s);

        }
    }
}

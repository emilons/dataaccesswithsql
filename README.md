# DataAccessWithSQL

This project is my submission to the second assignment in the Noroff Accelerate .NET course.

- The first part of the assignment is to implement various sql scripts.

- The second part is to implement a console application which connects to an existing database in order to retrieve and modify data.

# Project tree

**sql_scripts** - Contains the sql scripts implemented in the first part of the assignment.

**Chinook** - Contains the C# console application accessing and modifying the provided Chinook database.

```bash
├───Chinook                          # C# console application.
│   └───Chinook
│       ├───bin
│       │   └───Debug
│       │       └───net5.0
│       │           ├───ref
│       │           └───runtimes
│       ├───Data Access              # Methods containing the functionality which retrieves data from, or alters data in, the database.
│       ├───Models                   # Models of data retrieved from database.
│       └───obj
│           └───Debug
│               └───net5.0
│                   └───ref
└───sql_scripts                      # Sql scripts.
```

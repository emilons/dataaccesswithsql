CREATE TABLE Superhero(
	id int IDENTITY(1,1) PRIMARY KEY,
	full_name nvarchar(255),
	alias nvarchar(255),
	origin nvarchar(255)
);

CREATE TABLE Assistant (
	id int IDENTITY(1,1) PRIMARY KEY,
	full_name nvarchar(255)
);

CREATE TABLE Superpower (
	id int IDENTITY(1,1) PRIMARY KEY,
	power_name nvarchar(255),
	summary nvarchar(255)
);
CREATE TABLE SuperheroPowerJunction(
	superhero_id int,
	superpower_id int,
	CONSTRAINT SuperheroPowerId PRIMARY KEY (superhero_id, superpower_id),
	CONSTRAINT fk_superhero_id 
		FOREIGN KEY (superhero_id) references Superhero (id),
	CONSTRAINT fk_power_id 
		FOREIGN KEY (superpower_id) references Superpower (id)
);
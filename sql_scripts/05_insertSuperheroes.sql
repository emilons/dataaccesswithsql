INSERT INTO Superhero(full_name, alias, origin)
	values
	('Clark Kent', 'Superman', 'Metropolis'),
	('Bruce Wayne', 'Batman', 'Gotham'),
	('Barry Allen', 'The Flash', 'Central City');
INSERT INTO Superpower(power_name, summary)
	values
	('Super Strength', 'User can lift almost 100 kilos of feathers and steel.'),
	('Super Speed', 'User is a really speedy, almost as fast as SANIC'),
	('Rich', 'User got dem dollas $$$'),
	('Flying', 'User likes birds weeeee');

INSERT INTO SuperheroPowerJunction(superhero_id, superpower_id)
	values
	((SELECT ID FROM Superhero WHERE alias = 'Superman' ),(SELECT ID FROM Superpower WHERE power_name = 'Super Strength')),
	((SELECT ID FROM Superhero WHERE alias = 'Superman' ),(SELECT ID FROM Superpower WHERE power_name = 'Flying')),
	((SELECT ID FROM Superhero WHERE alias = 'Superman' ),(SELECT ID FROM Superpower WHERE power_name = 'Super Speed')),
	((SELECT ID FROM Superhero WHERE alias = 'Batman' ),(SELECT ID FROM Superpower WHERE power_name = 'Rich')),
	((SELECT ID FROM Superhero WHERE alias = 'The Flash' ),(SELECT ID FROM Superpower WHERE power_name = 'Super Speed'));
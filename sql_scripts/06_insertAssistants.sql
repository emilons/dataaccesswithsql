INSERT INTO Assistant(full_name, fk_superhero_id)
	values
	('Grogu', (SELECT ID FROM Superhero WHERE alias = 'Superman')),
	('R2D2', (SELECT ID FROM Superhero WHERE alias = 'Batman')),
	('Jar Jar Binks', (SELECT ID FROM Superhero WHERE alias = 'The Flash'));